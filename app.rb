require 'rubygems'
require 'sinatra'
require 'sqlite3'

def init_db 
	@db = SQLite3::Database.new 'blog.db'
	@db.results_as_hash = true
end

before do 
	init_db
end

configure do 
	init_db
	@db.execute 'create table if not exists posts 
	(
		id integer primary key autoincrement, 
		created_date date, 
		title text, 
		content text
		)'
	
	@db.execute 'create table if not exists comments 
	(
		id integer primary key autoincrement, 
		created_date date,
		content text,
		post_id integer
		)'
end

get '/' do

	@results = @db.execute 'select * from posts order by id desc' 

	erb :index
end

get '/new' do
  
	erb :new
end

post '/new' do
  title = params[:title]
	content = params[:content]
	
	if title.size <= 0 || content.size <= 0 
		@error = 'Type text'
		return erb :new
	end

	@db.execute 'insert into posts 
	(
		title, 
		content, 
		created_date
		) 
		values  
		(
			?, 
			?, 
			datetime()
			)', [title, content]

	redirect to '/'
end

get '/details/:post_id' do 
	post_id = params[:post_id]

	results = @db.execute 'select * from posts order by id = ?', [post_id] 

	@row = results[0]  

	@comments = @db.execute 'select * from comments where post_id = ? order by id', [post_id]
	
	erb :details
end

post '/details/:post_id' do 
	post_id = params[:post_id]
	content = params[:content]

	if content.size <= 0 
		@error = 'Type comment'
		return erb ''
	end

	@db.execute 'insert into comments 
	(
		content, 
		created_date, 
		post_id
		) 
		values  
		(
			?, 
			datetime(),
			?
	)', [content, post_id]

	redirect to('/details/' + post_id)
end